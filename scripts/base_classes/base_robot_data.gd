extends Resource
class_name BaseRobotData

export (String) var name
export (String, MULTILINE) var description

export (int) var base_max_health
export (int) var base_armor
export (int) var base_max_memory
export (int) var base_offensive_power
export (int) var base_defensive_power
export (int) var base_processing_power
