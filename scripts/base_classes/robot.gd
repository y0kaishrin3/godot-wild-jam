extends Resource
class_name Robot

export (Resource) var base_data

export (int) var bonus_max_memory
export (int) var bonus_max_health

export (int) var current_health

export (Array, Resource) var actions

