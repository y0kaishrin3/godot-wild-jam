extends Resource
class_name Action

enum TYPES {
	KINETIC,
	PSIONIC,
	PLASMIC,
	ELECTRIC,
	CRYOGENIC
}

enum CATEGORIES {
	OFFENSIVE,
	DEFENSIVE,
	UPGRADE,
	DOWNGRADE
}

export (int) var memory_cost
export (Resource) var effect
export (TYPES) var type
export (CATEGORIES) var category

