extends ActionEffect
class_name ChangeLife

export (int) var amount
export (bool) var is_healing

func execute(target: Robot):
	self.amount = self.amount * -1 if self.is_healing else self.amount
	if not self.is_healing and (self.target.current_health - self.amount) < 0:
		self.target.current_health = 0
	elif self.is_healing and (self.target.current_health - self.amount) > \
	self.target.base_data.max_health:
		self.target.current_health = self.target.base_data.max_health
	else:
		self.target.current_health -= self.amount
	
	
